﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE GetPlanFactByUsers
@WorkerIds nvarchar(255),
@Year int
AS
BEGIN
	--DECLARE @TableWorkerIds TABLE(element nvarchar(255),orderId int)
	--INSERT INTO @TableWorkerIds SELECT * FROM dbo.fn_ParseString(@WorkerIds,',')

	SELECT MAX(r.Name) as resource, MAX(p.ProjectName) as Project,  SUM(a.ActualWorkMinutes)/60 as [Plan], SUM(a.WorkMinutes)/60 as Fact FROM Projects p
JOIN Tasks t ON t.ProjectId=p.Id
JOIN Resources r ON t.ResourceId=r.Id
JOIN Assignments a ON a.TaskId=t.Id AND a.ResourceId=r.Id
WHERE a.IsDeleted=0 AND t.IsDeleted=0 AND r.IsDeleted=0 AND p.IsDeleted=0
AND r.Id IN (SELECT element FROM dbo.fn_ParseString(@WorkerIds,',')) 
AND DATEPART(YEAR, a.[Date]) = @Year
GROUP BY r.id, p.Id


END
