﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetResourceData]
@WorkerIds nvarchar(255),
@StartWeek int,
@EndWeek int,
@Year int
AS
BEGIN

	DECLARE @TableWorkerIds TABLE(element nvarchar(255),orderId int)
	INSERT INTO @TableWorkerIds SELECT * FROM dbo.fn_ParseString(@WorkerIds,',')


	SELECT r.Name, wbrt.Week, wbrt.WeekStart WeekStart, SUM(wbrt.WorkByTaskWeekly) Work FROM fWorkByTaskAndResourceWeekly() wbrt
	JOIN Resources r ON wbrt.ResourceId=r.Id
	WHERE datepart(YEAR, wbrt.WeekStart)=@Year AND wbrt.Week>=@StartWeek AND wbrt.Week<=@EndWeek AND r.Id IN (SELECT element FROM @TableWorkerIds) 
	GROUP BY r.Name,  wbrt.Week, wbrt.WeekStart





END
