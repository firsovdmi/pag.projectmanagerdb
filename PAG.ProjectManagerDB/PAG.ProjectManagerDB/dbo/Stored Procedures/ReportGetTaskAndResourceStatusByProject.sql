﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetTaskAndResourceStatusByProject]
@ProjectId int
AS
BEGIN

SELECT 
r.Name ResourceName
, t.Name TaskName
, dbo.fDevideFloats(awbt.ActualWorkByTaskWeekly, wbt.WorkByTaskWeekly) [Status]
, awbt.Week Week
, wbt.WeekStart WeekStart
FROM fWorkByTaskAndResourceWeekly() wbt
JOIN Tasks t ON wbt.TaskId=t.Id
JOIN Resources r ON wbt.ResourceId=r.Id
JOIN fActualWorkByTaskAndResourceWeekly() awbt ON awbt.TaskId=wbt.TaskId AND awbt.Week=wbt.Week
WHERE wbt.ProjectId=@ProjectId AND t.IsDeleted=0
ORDER BY wbt.WeekStart
END


