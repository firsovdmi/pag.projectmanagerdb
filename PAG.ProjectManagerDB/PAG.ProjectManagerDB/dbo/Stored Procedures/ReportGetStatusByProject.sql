﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetStatusByProject]
@ProjectId int
AS
BEGIN



WITH    rows AS
        (
        SELECT  *, ROW_NUMBER() OVER (ORDER BY [WeekStart]) AS rn
        FROM    fStatusByProjectWeekly() WHERE id=@ProjectId
        )
SELECT mp.Week,  mp.WeekStart, mp.StatusByProjectWeekly,  (mp.StatusByProjectWeekly - mc.StatusByProjectWeekly) Diff 
FROM    rows mc
RIGHT JOIN    rows mp
ON      mc.rn = mp.rn - 1

END
