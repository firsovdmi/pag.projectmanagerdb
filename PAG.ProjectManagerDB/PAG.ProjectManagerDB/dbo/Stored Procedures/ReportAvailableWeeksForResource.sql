﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportAvailableWeeksForResource]
@Year int,
@WorkerIds nvarchar(255)
AS
BEGIN

	declare @minWeek int
	declare @maxWeek int

	DECLARE @TableWorkerIds TABLE(element nvarchar(255),orderId int)
	INSERT INTO @TableWorkerIds SELECT * FROM dbo.fn_ParseString(@WorkerIds,',')

	SELECT @minWeek=MIN(Week), @maxWeek=MAX(Week) FROM dbo.fWeekAssigments() a WHERE DATEPART(YEAR, a.Date)=@Year AND a.IsDeleted=0 AND a.ResourceId IN (SELECT element FROM @TableWorkerIds) 
	SELECT num, num Text FROM dbo.fGetRangeBetweenTwoNumbers(@minWeek,@maxWeek)

END
