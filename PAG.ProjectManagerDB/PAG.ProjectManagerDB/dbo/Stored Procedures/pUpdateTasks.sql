﻿CREATE PROCEDURE [dbo].[pUpdateTasks] 
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE task
		SET task.PercentComplete = ISNULL(calc.PercentCompeted, 0),
			task.IsNeedUpdate = 0
		FROM dbo.Tasks as task
			CROSS APPLY 
			(SELECT
				 CASE 
				  WHEN AVG(t.PlanWork)<=0 THEN 0.0
				  WHEN SUM(a.ActualWorkMinutes/60) >= AVG(t.PlanWork) then 1.0
				  ELSE CAST(SUM(a.ActualWorkMinutes/60) as float)/AVG(t.PlanWork)
			 END as PercentCompeted 
			 FROM dbo.Tasks as t
					INNER JOIN Assignments a ON a.TaskId=t.Id
			 WHERE t.Id=task.Id) as calc
		where task.IsNeedUpdate = 1
END
