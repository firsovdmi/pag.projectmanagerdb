﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [ReportGetProjectStatus_old]
AS
BEGIN

SELECT  MAX(p.ProjectName) as Project,
 p.Start, 
 p.Finish, 
 p.CostWork,
 SUM(CAST(a.ActualWorkMinutes as float))/60 as [Plan],
  SUM(CAST(a.WorkMinutes as float))/60 as Fact, 
IIF(SUM(a.ActualWorkMinutes)>0, SUM(CAST(a.WorkMinutes as float))/SUM(CAST(a.ActualWorkMinutes as float)) *100,
  0.0) as PecentComplete  FROM Projects p
JOIN Tasks t ON t.ProjectId=p.Id
JOIN Assignments a ON a.TaskId=t.Id
WHERE a.IsDeleted=0 AND t.IsDeleted=0 AND p.IsDeleted=0
GROUP BY p.Id, p.Start, p.Finish, p.CostWork

END
