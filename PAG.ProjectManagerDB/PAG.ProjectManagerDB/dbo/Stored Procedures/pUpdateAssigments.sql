﻿CREATE PROCEDURE [dbo].[pUpdateAssigments] 
AS
BEGIN
	
	SET NOCOUNT ON;

	DECLARE @TableDeletedTasks TABLE(Id int)
	INSERT INTO @TableDeletedTasks SELECT Id FROM Tasks WHERE IsDeleted=1

    UPDATE Assignments 
		SET IsDeleted=1
		WHERE TaskId IN (SELECT Id FROM @TableDeletedTasks)
END
