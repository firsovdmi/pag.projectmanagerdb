﻿CREATE PROCEDURE [dbo].[ReportGetAvailableWorkers]

AS
BEGIN

SELECT r.id, r.Name FROM Resources r
JOIN Assignments a ON a.ResourceId=r.Id
WHERE r.IsDeleted=0 AND a.IsDeleted=0 --AND DATEPART(year,a.Date) = @Year 
GROUP BY r.Id, r.Name


END