﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE ReportGetConsolidated 
AS
BEGIN
	SELECT p.ProjectName ProjectName
	, dbo.fDevideInts(cwbp.CompleteWorkByProject, pbp.PlanByProject) [Status]
	, p.Start Start
	, p.Finish Finish
	, p.CostWork KP
	, pbp.PlanByProject [Plan]
	, cwbp.CompleteWorkByProject Completed
	, pbp.PlanByProject - cwbp.CompleteWorkByProject Remain
	, awbp.ActualWorkByProject ActualWork
	, dbo.fDevideInts(awbp.ActualWorkByProject, pbp.PlanByProject) ExpectedPercentageCompletion
	, dbo.fDevideFloats(dbo.fDevideInts(cwbp.CompleteWorkByProject, pbp.PlanByProject), dbo.fDevideInts(awbp.ActualWorkByProject, pbp.PlanByProject)) Efficiency
	FROM Projects p
	LEFT JOIN fCompleteWorkByProject() cwbp ON cwbp.Id=p.Id
	LEFT JOIN fPlanByProject() pbp ON pbp.Id=p.Id
	LEFT JOIN fActualWorkByProject() awbp ON awbp.Id=p.Id
END
