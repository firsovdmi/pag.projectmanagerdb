﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetKTU]
@WorkerIds nvarchar(255),
@ProjectIds nvarchar(255)
AS
BEGIN

	DECLARE @TableWorkerIds TABLE(element nvarchar(255),orderId int)
	INSERT INTO @TableWorkerIds SELECT * FROM dbo.fn_ParseString(@WorkerIds,',')
	DECLARE @TableProjectIds TABLE(element nvarchar(255),orderId int)
	INSERT INTO @TableProjectIds SELECT * FROM dbo.fn_ParseString(@ProjectIds,',')

SELECT 
-- Название проекта
  MAX(p.ProjectName) ProjectName
-- Имя ресурса		
, MAX(r.Name) ResourceName
-- Выполненная работа
, SUM(cwbt.CompleteWorkByTask) CompletedWork 
-- Потраченное время
, SUM(awbt.ActualWorkByTask) ActualWork	
-- Коэф. участия
, IIF(MAX(awbp.ActualWorkByProject)>0, CAST(SUM(awbt.ActualWorkByTask) as float)/CAST(MAX(awbp.ActualWorkByProject) as float), 0) ParticipationRate 
-- Эффективность
, IIF(SUM(awbt.ActualWorkByTask)>0, CAST(SUM(cwbt.CompleteWorkByTask) as float) / CAST(SUM(awbt.ActualWorkByTask) as float), 0) Efficiency
-- КТУ
, IIF(MAX(cwbp.CompleteWorkByProject)>0,  CAST(SUM(cwbt.CompleteWorkByTask) as float) / CAST(MAX(cwbp.CompleteWorkByProject) as float), 0) KTU

FROM 
Tasks t LEFT JOIN fCompleteWorkByTask() cwbt ON cwbt.Id=t.Id
JOIN Resources r ON t.ResourceId=r.Id
JOIN Projects p ON t.ProjectId=p.Id
LEFT JOIN fActualWorkByProject() awbp ON awbp.Id=p.Id
LEFT JOIN fActualWorkByTask() awbt ON awbt.Id=t.Id
LEFT JOIN fCompleteWorkByProject() cwbp ON cwbp.Id=p.Id
WHERE r.Id IN (SELECT element FROM @TableWorkerIds) 
AND p.Id IN  (SELECT element FROM @TableProjectIds) 
--and t.IsDeleted=0
GROUP BY p.Id, r.Id 
ORDER BY p.Id, r.Id
END
