﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetResourceByProjectsAndWeeks]
@WorkerIds nvarchar(255),
@StartWeek int,
@EndWeek int,
@Year int
AS
BEGIN

	DECLARE @TableWorkerIds TABLE(element nvarchar(255),orderId int)
	INSERT INTO @TableWorkerIds SELECT * FROM dbo.fn_ParseString(@WorkerIds,',')


	SELECT r.Name, p.ProjectName, awbrt.Week, awbrt.WeekStart WeekStart, SUM(awbrt.ActualWorkByTaskWeekly) Work FROM fActualWorkByTaskAndResourceWeekly() awbrt
	JOIN Resources r ON awbrt.ResourceId=r.Id
	JOIN Projects p ON awbrt.ProjectId=p.Id

	WHERE datepart(YEAR, awbrt.WeekStart)=@Year AND awbrt.Week>=@StartWeek AND awbrt.Week<=@EndWeek AND r.Id IN (SELECT element FROM @TableWorkerIds) 
	GROUP BY r.Name, p.ProjectName,  awbrt.Week, awbrt.WeekStart





END
