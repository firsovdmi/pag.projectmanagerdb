﻿CREATE PROCEDURE [dbo].[pUpdateProjects] 
AS
BEGIN
	
	SET NOCOUNT ON;

    UPDATE project
		SET project.PercentComplete = ISNULL(calc.PercentCompeted, 0),
			project.IsNeedUpdate = 0
		FROM dbo.Projects as project
			CROSS APPLY 
				(SELECT
					 CASE 
					  WHEN SUM(t.PlanWork) <= 0 THEN 0.0
					  WHEN SUM(a.ActualWorkMinutes / 60) >= SUM(t.PlanWork) then 1.0
					  ELSE CAST(SUM(a.ActualWorkMinutes/60) as float) / SUM(t.PlanWork)
				 END as PercentCompeted 
				 FROM dbo.Tasks as t
						INNER JOIN Assignments a ON a.TaskId = t.Id
				 WHERE t.ProjectId = project.Id) as calc
		where project.IsDeleted = 0 and project.IsNeedUpdate = 1
END
