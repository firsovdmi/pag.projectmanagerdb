﻿CREATE PROCEDURE [dbo].[pUpdateFromProject]
AS
BEGIN
	
	SET NOCOUNT ON;

    BEGIN TRANSACTION UpdateFromProject;

		EXEC dbo.pUpdateProjects
		EXEC dbo.pUpdateTasks
		EXEC dbo.pUpdateAssigments
	COMMIT TRANSACTION UpdateFromProject;
END
