﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[ReportGetAvailableYears]
AS
BEGIN

    -- Insert statements for procedure here
	SELECT DATEPART(YEAR, Date) AS Year FROM Assignments WHERE IsDeleted=0 GROUP BY DATEPART(YEAR, Date)
END
