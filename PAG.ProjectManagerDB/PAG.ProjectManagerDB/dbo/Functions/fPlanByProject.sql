﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- Plan = Сумма(PlanWork)= Сумма(PlanWork) = Сумма(Объем, часов)
CREATE FUNCTION [dbo].[fPlanByProject]()

RETURNS TABLE
AS
RETURN
	SELECT p.Id, SUM(t.PlanWork ) PlanByProject FROM 
	Tasks t JOIN Projects p ON t.ProjectId=p.Id
	where t.IsDeleted=0
	GROUP BY p.Id
