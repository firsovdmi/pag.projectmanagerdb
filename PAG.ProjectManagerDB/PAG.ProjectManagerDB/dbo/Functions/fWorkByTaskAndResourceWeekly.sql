﻿CREATE FUNCTION [dbo].[fWorkByTaskAndResourceWeekly]()

RETURNS TABLE
AS
RETURN
SELECT TOP 10000
t.Id TaskId
, t.ResourceId ResourceId
, MAX(t.ProjectId) ProjectId
, a.Week
, SUM(a.WorkMinutes/60) WorkByTaskWeekly
, DATEADD(dd, -(DATEPART(dw, MIN(a.Date))-1), MIN(a.Date)) [WeekStart] 
FROM Tasks t
JOIN dbo.fWeekAssigments() a ON a.TaskId=t.Id
WHERE a.IsDeleted=0
--and t.IsDeleted=0
GROUP BY t.Id, t.ResourceId, a.Week
ORDER BY [WeekStart]