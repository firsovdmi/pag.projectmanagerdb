﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- StatusWeekly = ActualWork / Work 
CREATE FUNCTION [dbo].[fStatusByProjectWeekly]()
RETURNS TABLE
AS
RETURN
	SELECT TOP 10000
	p.Id
	, wbtw.Week
	, dbo.fDevideInts( SUM(awbtw.ActualWorkByTaskWeekly), SUM(wbtw.WorkByTaskWeekly)) StatusByProjectWeekly 
	, DATEADD(dd, -(DATEPART(dw, MIN(wbtw.Date))-1), MIN(wbtw.Date)) [WeekStart] 
	, MAX(p.ProjectName) ProjectName
	FROM Projects p
	JOIN Tasks t ON t.ProjectId=p.Id
	LEFT JOIN fWorkByTaskWeekly() wbtw ON wbtw.Id=t.Id
	LEFT JOIN fActualWorkByTaskWeekly() awbtw ON awbtw.Id=t.Id AND awbtw.Week=wbtw.Week
	WHERE wbtw.Week IS NOT NULL AND wbtw.Week>0 AND t.IsDeleted=0
	GROUP BY p.Id, wbtw.Week
	ORDER BY [WeekStart]



