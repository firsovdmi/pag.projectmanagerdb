﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fActualWorkByProject]()

RETURNS TABLE
AS
RETURN
SELECT p.Id, SUM(awbt.ActualWorkByTask) ActualWorkByProject FROM Projects p
JOIN Tasks t ON t.ProjectId=p.Id
LEFT JOIN fActualWorkByTask() awbt ON awbt.Id=t.Id
GROUP BY p.Id
