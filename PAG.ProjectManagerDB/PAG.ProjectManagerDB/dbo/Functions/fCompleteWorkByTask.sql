﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- CompleteWork = PlanWork * PercentUserComplete = (Объем, часов) * (Оценка)
CREATE FUNCTION [dbo].[fCompleteWorkByTask]()

RETURNS TABLE
AS
RETURN
SELECT Id, (PlanWork * PercentUserComplete) CompleteWorkByTask FROM Tasks
