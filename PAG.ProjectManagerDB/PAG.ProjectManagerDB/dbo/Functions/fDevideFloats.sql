﻿CREATE FUNCTION [dbo].[fDevideFloats](@v1 float, @v2 float)
RETURNS float
BEGIN
 
RETURN IIF(@v2>0, CAST(@v1 as float) / CAST(@v2 as float), 0)
END