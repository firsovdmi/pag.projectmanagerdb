﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[fActualWorkByTask]()

RETURNS TABLE
AS
RETURN
SELECT t.Id, SUM(ROUND(a.ActualWorkMinutes/60,2)) ActualWorkByTask FROM Tasks t
JOIN Assignments a ON a.TaskId=t.Id
WHERE a.IsDeleted=0
--AND t.IsDeleted = 0
GROUP BY t.Id