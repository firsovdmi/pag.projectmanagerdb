﻿CREATE FUNCTION [dbo].[fDevideInts](@v1 int, @v2 int)
RETURNS float
BEGIN
 
RETURN IIF(@v2>0, CAST(@v1 as float) / CAST(@v2 as float), 0)
END