﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- CompleteWork = Сумма(CompleteWorkByTask)= Сумма(PlanWork * PercentUserComplete) = Сумма((Объем, часов) * (Оценка))
CREATE FUNCTION [dbo].[fCompleteWorkByProject]()

RETURNS TABLE
AS
RETURN
	SELECT p.Id, SUM(cwbt.CompleteWorkByTask) CompleteWorkByProject FROM 
	Tasks t JOIN fCompleteWorkByTask() cwbt ON cwbt.id=t.Id
	JOIN Projects p ON t.ProjectId=p.Id
	GROUP BY p.Id
