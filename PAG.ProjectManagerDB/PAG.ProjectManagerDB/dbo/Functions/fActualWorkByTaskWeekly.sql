﻿CREATE FUNCTION [dbo].[fActualWorkByTaskWeekly]()

RETURNS TABLE
AS
RETURN
SELECT t.Id,a.Week, SUM(ROUND(a.ActualWorkMinutes/60,2)) ActualWorkByTaskWeekly, MIN(a.Date) Date FROM Tasks t
JOIN dbo.fWeekAssigments() a ON a.TaskId=t.Id
WHERE a.IsDeleted=0
--and t.IsDeleted=0
GROUP BY t.Id, a.Week