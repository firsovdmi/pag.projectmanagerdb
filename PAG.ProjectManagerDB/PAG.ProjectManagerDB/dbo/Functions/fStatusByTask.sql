﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

-- Status = CompleteWork / PlanWork   ( (Объем, часов) * (Оценка) )/(Объем, часов)
CREATE FUNCTION [dbo].[fStatusByTask]()

RETURNS TABLE
AS
RETURN
SELECT Id, (PlanWork * PercentUserComplete) CompleteWorkByTask FROM Tasks
