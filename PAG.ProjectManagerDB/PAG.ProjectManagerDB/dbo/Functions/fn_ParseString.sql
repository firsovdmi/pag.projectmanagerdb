﻿CREATE function [dbo].[fn_ParseString] 
(
	@stringToParse nvarchar (max),
	@separator char(1)
)
RETURNS 
@Result TABLE 
(
	element nvarchar(max),orderId int
)
AS
BEGIN
declare @start int,@index int
declare @element nvarchar (max)
declare @lastChar char(1)
declare @orderId int
select @lastChar=substring(@stringToParse,len(@stringToParse),1)
if (@separator!=@lastChar) set @stringToParse=@stringToParse+@separator
select @start=1
select @orderId=0
	while (1>0)
	begin
		select @index=CHARINDEX ( @separator ,@stringToParse , @start ) 
		if @index=0 break
		select @element=substring(@stringToParse,@start,@index-@start)
		select @start=@start+len(@element)+1
		select @orderId=@orderId+1
		insert into @Result(element,orderId) values(@element,@orderId)
	end
	RETURN 
END