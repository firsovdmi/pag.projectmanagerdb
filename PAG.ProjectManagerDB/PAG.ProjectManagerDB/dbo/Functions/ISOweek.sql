﻿CREATE FUNCTION [dbo].[ISOweek]  (@DATE DATETIME)
RETURNS INT
AS
BEGIN
	declare @Week int = isnull(   datepart(iso_week,@DATE), 0    )

   RETURN  IIF( @Week>50 AND datepart(MONTH,@DATE)=1,1, @Week)
END
