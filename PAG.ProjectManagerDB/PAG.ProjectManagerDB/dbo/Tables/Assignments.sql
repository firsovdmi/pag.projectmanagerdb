﻿CREATE TABLE [dbo].[Assignments] (
    [Id]                INT        IDENTITY (1, 1) NOT NULL,
    [ProjectId]         INT        NOT NULL,
    [TaskId]            INT        NOT NULL,
    [ResourceId]        INT        NOT NULL,
    [Work]              FLOAT (53) CONSTRAINT [DF_Assignments_Work] DEFAULT ((0)) NOT NULL,
    [ActualWork]        FLOAT (53) CONSTRAINT [DF_Assignments_ActualWork] DEFAULT ((0)) NOT NULL,
    [Date]              DATE       CONSTRAINT [DF_Assignments_Date] DEFAULT (getdate()) NOT NULL,
    [IsDeleted]         BIT        CONSTRAINT [DF_Assignments_IsDeleted] DEFAULT ((0)) NOT NULL,
    [Year]              AS         (isnull(datepart(year,[Date]),(0))) PERSISTED NOT NULL,
    [Month]             AS         (isnull(datepart(month,[Date]),(0))) PERSISTED NOT NULL,
    [IsNeedUpdate]      BIT        CONSTRAINT [DF_Assignments_IsNeedUpdate] DEFAULT ((0)) NOT NULL,
    [WorkMinutes]       INT        CONSTRAINT [DF_Assignments_WorkMinutes] DEFAULT ((0)) NOT NULL,
    [ActualWorkMinutes] INT        CONSTRAINT [DF_Assignments_ActualWorkMinutes] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Assignments] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Assignments_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([Id]),
    CONSTRAINT [FK_Assignments_Resources] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[Resources] ([Id]),
    CONSTRAINT [FK_Assignments_Tasks] FOREIGN KEY ([TaskId]) REFERENCES [dbo].[Tasks] ([Id])
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_Assignments_Unique]
    ON [dbo].[Assignments]([ProjectId] ASC, [TaskId] ASC, [ResourceId] ASC, [Date] ASC, [IsDeleted] ASC) WHERE ([IsDeleted]=(0));


GO
CREATE NONCLUSTERED INDEX [IX_Assignments_IsDeleted_IsNeedUpdate]
    ON [dbo].[Assignments]([IsDeleted] ASC, [IsNeedUpdate] ASC)
    INCLUDE([TaskId], [Id], [ProjectId], [ResourceId]);


GO
CREATE NONCLUSTERED INDEX [IX_Assignments_IsDeleted]
    ON [dbo].[Assignments]([IsDeleted] ASC)
    INCLUDE([TaskId], [Id], [ProjectId], [ResourceId], [IsNeedUpdate]);


GO
CREATE TRIGGER [dbo].[Assignments_update] ON dbo.Assignments
FOR UPDATE
AS 
UPDATE t
SET t.IsNeedUpdate = 1
FROM dbo.Tasks as t
	INNER JOIN dbo.Assignments as a ON t.Id = a.TaskId
WHERE a.IsDeleted = 0 AND a.IsNeedUpdate = 1 and t.IsDeleted = 0
