﻿CREATE TABLE [dbo].[SkillsBind] (
    [Id]           INT IDENTITY (1, 1) NOT NULL,
    [ResourceId]   INT NOT NULL,
    [SkillLevelId] INT NOT NULL,
    [IsDeleted]    BIT CONSTRAINT [DF_SkillsBind_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SkillsBind] PRIMARY KEY CLUSTERED ([Id] ASC)
);

