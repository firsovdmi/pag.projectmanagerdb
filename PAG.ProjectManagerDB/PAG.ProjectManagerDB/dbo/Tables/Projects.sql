﻿CREATE TABLE [dbo].[Projects] (
    [Id]              INT            IDENTITY (1, 1) NOT NULL,
    [ProjectName]     NVARCHAR (255) NOT NULL,
    [CostWork]        INT            NULL,
    [Manager]         INT            NULL,
    [Start]           DATE           NULL,
    [Finish]          DATE           NULL,
    [CurrentDate]     DATE           NULL,
    [PercentComplete] FLOAT (53)     NULL,
    [Work]            INT            NULL,
    [ActualWork]      INT            NULL,
    [IsDeleted]       BIT            CONSTRAINT [DF_Projects_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsNeedUpdate]    BIT            CONSTRAINT [DF_Projects_IsNeedUpdate] DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_Projects] PRIMARY KEY CLUSTERED ([Id] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_Projects_IsDeleted]
    ON [dbo].[Projects]([IsDeleted] ASC)
    INCLUDE([Id], [IsNeedUpdate]);


GO
CREATE NONCLUSTERED INDEX [IX_Projects_IsDeleted_IsNeedUpdate]
    ON [dbo].[Projects]([IsDeleted] ASC, [IsNeedUpdate] ASC)
    INCLUDE([Id], [PercentComplete]);

