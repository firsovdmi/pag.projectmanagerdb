﻿CREATE TABLE [dbo].[SkillLevels] (
    [Id]          INT         IDENTITY (1, 1) NOT NULL,
    [SkillId]     INT         NOT NULL,
    [SkillLevel]  INT         NOT NULL,
    [Description] NCHAR (255) NOT NULL,
    [IsDeleted]   BIT         CONSTRAINT [DF_SkillLevels_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_SkillLevels] PRIMARY KEY CLUSTERED ([Id] ASC)
);

