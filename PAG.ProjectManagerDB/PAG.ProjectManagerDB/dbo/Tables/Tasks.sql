﻿CREATE TABLE [dbo].[Tasks] (
    [Id]                  INT             IDENTITY (1, 1) NOT NULL,
    [TaskUID]             CHAR (36)       NULL,
    [ProjectId]           INT             CONSTRAINT [DF_Tasks_ProjectId] DEFAULT ((0)) NOT NULL,
    [ResourceId]          INT             CONSTRAINT [DF_Tasks_ResourceId] DEFAULT ((0)) NULL,
    [PercentComplete]     FLOAT (53)      CONSTRAINT [DF_Tasks_PercentComplete] DEFAULT ((0)) NOT NULL,
    [PercentUserComplete] FLOAT (53)      CONSTRAINT [DF_Tasks_PercentUserComplete] DEFAULT ((0)) NOT NULL,
    [Priority]            INT             CONSTRAINT [DF_Tasks_Priority] DEFAULT ((0)) NOT NULL,
    [Start]               DATE            CONSTRAINT [DF_Tasks_Start] DEFAULT ('01.01.1900') NOT NULL,
    [Finish]              DATE            CONSTRAINT [DF_Tasks_Finish] DEFAULT ('01.01.1900') NOT NULL,
    [Work]                INT             CONSTRAINT [DF_Tasks_Work] DEFAULT ((0)) NOT NULL,
    [ActualWork]          INT             CONSTRAINT [DF_Tasks_ActualWork] DEFAULT ((0)) NOT NULL,
    [PlanWork]            INT             CONSTRAINT [DF_Tasks_PlanWork] DEFAULT ((0)) NOT NULL,
    [Description]         NVARCHAR (4000) CONSTRAINT [DF_Tasks_Description] DEFAULT ('') NOT NULL,
    [WorkerDescription]   NVARCHAR (4000) NULL,
    [Path]                NVARCHAR (4000) CONSTRAINT [DF_Tasks_Path] DEFAULT ('') NOT NULL,
    [FullName]            AS              ([Path]+[Name]) PERSISTED NOT NULL,
    [Name]                NVARCHAR (4000) CONSTRAINT [DF_Tasks_Name] DEFAULT ('') NOT NULL,
    [IsDeleted]           BIT             CONSTRAINT [DF_Tasks_IsDeleted] DEFAULT ((0)) NOT NULL,
    [IsClosed]            BIT             CONSTRAINT [DF_Tasks_IsClosed] DEFAULT ((0)) NOT NULL,
    [IsNeedUpdate]        BIT             CONSTRAINT [DF_Tasks_IsNeedUpdate] DEFAULT ((1)) NOT NULL,
    [IsHidden]            BIT             CONSTRAINT [DF_Tasks_IsHidden] DEFAULT ((0)) NOT NULL,
    [SkillId]             INT             NULL,
    [SkillLevel]          INT             NULL,
    [TaskCreater]         NVARCHAR (4000) CONSTRAINT [DF_Tasks_TaskCreater] DEFAULT ('') NOT NULL,
    CONSTRAINT [PK_Tasks] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_Tasks_Projects] FOREIGN KEY ([ProjectId]) REFERENCES [dbo].[Projects] ([Id]),
    CONSTRAINT [FK_Tasks_Resources] FOREIGN KEY ([ResourceId]) REFERENCES [dbo].[Resources] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_Tasks_IsDeleted_IsNeedUpdate]
    ON [dbo].[Tasks]([IsDeleted] ASC, [IsNeedUpdate] ASC)
    INCLUDE([Id], [ProjectId], [ResourceId]);


GO
CREATE NONCLUSTERED INDEX [IX_Tasks_IsDeleted]
    ON [dbo].[Tasks]([IsDeleted] ASC)
    INCLUDE([Id], [ProjectId], [ResourceId], [IsNeedUpdate]);


GO
CREATE TRIGGER [dbo].[Tasks_update] ON dbo.Tasks
FOR UPDATE
AS 
UPDATE p
SET p.IsNeedUpdate = 1
FROM dbo.Projects as p
	INNER JOIN dbo.Tasks as t ON p.Id = t.ProjectId
WHERE t.IsDeleted = 0 AND t.IsNeedUpdate = 1 and p.IsDeleted = 0
