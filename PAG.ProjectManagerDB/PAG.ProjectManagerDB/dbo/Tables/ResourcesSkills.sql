﻿CREATE TABLE [dbo].[ResourcesSkills] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [Name]      NVARCHAR (255) NOT NULL,
    [IsDeleted] BIT            CONSTRAINT [DF_ResourcesSkills_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ResourcesSkills] PRIMARY KEY CLUSTERED ([Id] ASC)
);

