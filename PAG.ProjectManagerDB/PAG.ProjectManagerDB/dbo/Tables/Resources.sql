﻿CREATE TABLE [dbo].[Resources] (
    [Id]        INT            IDENTITY (1, 1) NOT NULL,
    [FullName]  NVARCHAR (255) NULL,
    [Name]      NVARCHAR (255) NOT NULL,
    [WorkerId]  INT            NULL,
    [IsDeleted] BIT            CONSTRAINT [DF_Resources_IsDeleted] DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_Resources] PRIMARY KEY CLUSTERED ([Id] ASC)
);

