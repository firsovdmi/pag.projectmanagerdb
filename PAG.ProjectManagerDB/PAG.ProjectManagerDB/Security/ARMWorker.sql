﻿CREATE ROLE [ARMWorker]
    AUTHORIZATION [dbo];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\miroshnikov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\uryadov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\ideco4];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\dane4ka];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\toshchev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\surovtsev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\seliverstov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\simakova];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\grishin];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kuznetsov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\bitar];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kazakov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\glushkova];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\makarovsky];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\galiyev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kazantsev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\krokhotin];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\suslov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\fisun];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\ogurtsov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\churanov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\belova];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\osenenko];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\yegorov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\borovkov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\ziad];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\scan];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\lobanov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\sedova];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\volkov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\sheshenin];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\arkhipov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\taranenko];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\ott];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\fisenko];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\maria];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kopylov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\pereskok];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\yurov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\im87];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\shirinskiy];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kuznetsova];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\khakharev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kubarev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\shkarin];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\kamaltdinov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\orekhov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\balabanov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\sarychev];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\yakubov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\okulov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\zadavin];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\lashko];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\ershov];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\dane4ka2];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\a.bitar];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\gretskaya];


GO
ALTER ROLE [ARMWorker] ADD MEMBER [PAG\Baryshnikov];

